// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.Telephony;

public class ClassCallsTexts {

	private static final String TAG = "AMoSSCallsTexts";

	private Context mContext;
	private File mFile;
	private BufferedWriter mWriter;
	private long mTsPrev;
	private CallsObserver mCalls;
	private SmsMmsObserver mSmsMms;
	private ContentResolver mResolver;

	public ClassCallsTexts(Context context)
	{
		mContext = context;
	}

	// Open for write
	public void init(long ts)
	{
		// TODO: Fix it to check and wait for storage
		try {
			File root = Environment.getExternalStorageDirectory();
			if (root.canWrite()){
				// Create folders
				File folder = new File(root, ClassConsts.FILES_ROOT);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				// Initialise the main calls & texts file
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
				fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
				mFile = new File(folder, "calls-sms-mms-" + fmt.format(new Date(ts)) + ".csv");
				mWriter = new BufferedWriter(new FileWriter(mFile, true));
				
				File[] files = folder.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return (name.toLowerCase().matches("^calls-sms-mms-.*csv$") &&
								!name.toLowerCase().equals(mFile.getName().toLowerCase()));
					}
				});
				if (files.length > 0) {
					new TaskZipper().execute(files);
				}
			} else {
				new ClassEvents(TAG, "ERROR", "SD card not writable");
			}
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not open file " + e.getMessage());
		}
		mTsPrev = ts;

		mResolver = mContext.getContentResolver();
		Handler handler = new Handler();
		mCalls = new CallsObserver(handler);
		mSmsMms = new SmsMmsObserver(handler);
		
		try {
			mResolver.registerContentObserver(CallLog.Calls.CONTENT_URI, true, mCalls);
		} catch(Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not register call log content observer " + e.getMessage());
		}
		try {
			mResolver.registerContentObserver(Uri.parse("content://mms-sms/"), true, mSmsMms);
		} catch(Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not register SMS/MMS log content observer " + e.getMessage());
		}
	}

	public void fini()
	{
		try {
			mResolver.unregisterContentObserver(mCalls);
		} catch(Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not unregister call log content observer " + e.getMessage());
		}
		try {
			mResolver.unregisterContentObserver(mSmsMms);
		} catch(Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not unregister SMS/MMS log content observer " + e.getMessage());
		}
		try {
			if (mWriter != null) {
				mWriter.close();
				mWriter = null;
			}
			if (mFile != null) {
				mFile = null;
			}
		} catch (Exception e) {
			new ClassEvents(TAG, "ERROR", "Could not close file " + e.getMessage());
		}
	}

	void update(long ts, String type, int id, String dir, List<String> numberList, int len)
	{
		ListIterator<String> numberIterator = numberList.listIterator();
		while (numberIterator.hasNext()) {
			update(ts, type, id, dir, numberIterator.next(), len);
		}
	}

	void update(long ts, String type, int id, String dir, String num, int len)
	{
		long tsNow = System.currentTimeMillis();
		
		long today = tsNow/ClassConsts.MILLIDAY;
		long yesterday = mTsPrev/ClassConsts.MILLIDAY;
		if (today > yesterday) {
			File f = mFile;
			fini();
			new TaskZipper().execute(f);
			init(tsNow);
		}
		if (mWriter != null) {
			try {
				num = num.replaceFirst("^\\+44", "0");
				String numHash = md5(num);
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				String str = fmt.format(new Date(ts)) + 
						"," + type + "," + id + "," + dir +"," + numHash +
						"," + len + "\n";
				mWriter.write(str);
				// TODO: We may not need to flush that often
				mWriter.flush();
			} catch (Exception e) {
				new ClassEvents(TAG, "ERROR", "Could not write file " + e.getMessage());
			}
		}
		mTsPrev = tsNow;
	}

	class CallsObserver extends ContentObserver
	{
		public CallsObserver(Handler handler) 
		{
			super(handler);
		}

		@Override
		public void onChange(boolean bSelfChange)
		{
			super.onChange(bSelfChange);

			Cursor calls_cursor = null;
			
			try {
				calls_cursor = mContext.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DEFAULT_SORT_ORDER);
				
				if (calls_cursor.moveToFirst()) {
					long ts = calls_cursor.getLong(calls_cursor.getColumnIndex("date"));
					int id = calls_cursor.getInt(calls_cursor.getColumnIndex("_id"));
					int duration = calls_cursor.getInt(calls_cursor.getColumnIndex("duration"));
					int type = calls_cursor.getInt(calls_cursor.getColumnIndex("type"));
					String num = calls_cursor.getString(calls_cursor.getColumnIndex("number"));
		
					if (type == CallLog.Calls.INCOMING_TYPE) {
						update(ts, "Call", id, "I", num, duration);
					} else if (type == CallLog.Calls.OUTGOING_TYPE) {
						update(ts, "Call", id, "O", num, duration);
					} else if (type == CallLog.Calls.MISSED_TYPE) {
						update(ts, "Call", id, "I", num, 0);
					} else {
						// Incoming call sent to voicemail, probably
						update(ts, "Call", id, String.valueOf(type), num, duration);
					}
				}
			} catch(Exception e) {
				new ClassEvents(TAG, "ERROR", "Could not read call log " + e.getMessage());
			} finally {
				closeCursor(calls_cursor);
			}
		}

		@Override
		public boolean deliverSelfNotifications() {
		    return true;
		}
	}

	class SmsMmsObserver extends ContentObserver
	{
		public SmsMmsObserver(Handler handler) {
			super(handler);
		}

		@SuppressLint("InlinedApi") @Override
		public void onChange(boolean bSelfChange)
		{    
			super.onChange(bSelfChange);
			
			long sms_ts = 0;
			long sms_ts_compare = 0;
			int sms_id, sms_length, sms_type;
			String sms_number = "";
			
			long mms_ts = 0;
			long mms_ts_compare = 0;
			int mms_id, mms_type, mms_size;
			LinkedList<String> mms_number_from = new LinkedList<String>();
			LinkedList<String> mms_number_to = new LinkedList<String>();
			
			Cursor sms_cursor = null;
			Cursor mms_cursor = null;
			
			try {
				sms_cursor = mContext.getContentResolver().query(Telephony.Sms.CONTENT_URI, null, null, null, Telephony.Sms.DEFAULT_SORT_ORDER);
				
				if (sms_cursor.moveToFirst()) {
					sms_ts = sms_cursor.getLong(sms_cursor.getColumnIndex("date"));
					sms_ts_compare = sms_ts / 1000;
				}
			} catch (Exception e) {
				new ClassEvents(TAG, "ERROR", "Could not open SMS cursor " + e.getMessage());
				
				sms_ts = 0;
				sms_ts_compare = 0;
			}
			
			try {
				mms_cursor = mContext.getContentResolver().query(Telephony.Mms.CONTENT_URI, null, null, null, "date DESC");
				
				if (mms_cursor.moveToFirst()) {
					mms_ts_compare = mms_cursor.getLong(mms_cursor.getColumnIndex("date"));
					mms_ts = mms_ts_compare * 1000;
				}
			} catch (Exception e) {
				new ClassEvents(TAG, "ERROR", "Could not open MMS cursor " + e.getMessage());
				
				mms_ts = 0;
				mms_ts_compare = 0;
			}
			
			if ((sms_ts == 0) && (mms_ts == 0)) {
				// nothing to do.
			} else {
				// if mms_ts_compare == sms_ts_compare then both of these will run
				// and output both message types.
				if (sms_ts_compare >= mms_ts_compare) {
					try {
						sms_id = sms_cursor.getInt(sms_cursor.getColumnIndex("_id"));
						sms_length = sms_cursor.getString(sms_cursor.getColumnIndex("body")).length();
						sms_type = sms_cursor.getInt(sms_cursor.getColumnIndex("type"));
						sms_number = sms_cursor.getString(sms_cursor.getColumnIndex("address"));
						
						if (sms_type == Telephony.Sms.MESSAGE_TYPE_INBOX) {
							update(sms_ts, "SMS", sms_id, "I", sms_number, sms_length);
						} else if ((sms_type == Telephony.Sms.MESSAGE_TYPE_OUTBOX) ||
								(sms_type == Telephony.Sms.MESSAGE_TYPE_QUEUED) ||
								(sms_type == Telephony.Sms.MESSAGE_TYPE_SENT)) {
							update(sms_ts, "SMS", sms_id, "O", sms_number, sms_length);
						} else {
							// Ignore MESSAGE_TYPE_DRAFT and MESSAGE_TYPE_FAILED.
						}
					} catch (Exception e) {
						new ClassEvents(TAG, "ERROR", "Could not read SMS log " + e.getMessage());
					}
				}
				
				if (mms_ts_compare >= sms_ts_compare) {
					Cursor mms_cursor_addr = null;
					
					try {
						mms_id = mms_cursor.getInt(mms_cursor.getColumnIndex("_id"));
						mms_type = mms_cursor.getInt(mms_cursor.getColumnIndex("msg_box"));
						mms_size = mms_cursor.getInt(mms_cursor.getColumnIndex("m_size"));
						
						Uri uriAddr = Uri.parse(String.format("content://mms/%d/addr", mms_id));
						
						mms_cursor_addr = mContext.getContentResolver().query(uriAddr, null, "msg_id=" + mms_id, null, null);
						
						if (mms_cursor_addr.moveToFirst()) {
							do {
								String this_mms_address = mms_cursor_addr.getString(mms_cursor_addr.getColumnIndex("address"));
								int this_mms_address_type = mms_cursor_addr.getInt(mms_cursor_addr.getColumnIndex("type"));
								
								if (this_mms_address != null) {
									this_mms_address = this_mms_address.replace("-", "");
									
									if ((this_mms_address_type == 0x97) ||      /* PduHeaders.TO */
											(this_mms_address_type == 0x82) ||  /* PduHeaders.CC */
											(this_mms_address_type == 0x81)) {  /* PduHeaders.BCC */
										mms_number_to.add(this_mms_address);
									} else if (this_mms_address_type == 0x89) { /* PduHeaders.FROM */
										mms_number_from.add(this_mms_address);
									}
								}
							} while (mms_cursor_addr.moveToNext());
						}
						
						if (mms_size > 0) {
							if ((mms_type == Telephony.Mms.MESSAGE_BOX_INBOX) &&
									(mms_number_from.size() > 0)) {
								update(mms_ts, "MMS", mms_id, "I", mms_number_from, mms_size);
							} else if ((mms_type == Telephony.Mms.MESSAGE_BOX_OUTBOX) ||
									(mms_type == Telephony.Mms.MESSAGE_BOX_SENT) &&
									(mms_number_to.size() > 0)) {
								update(mms_ts, "MMS", mms_id, "O", mms_number_to, mms_size);
							} else {
								// Ignore MESSAGE_BOX_DRAFT and MESSAGE_BOX_FAILED.
							}
						}
					} catch (Exception e) {
						new ClassEvents(TAG, "ERROR", "Could not read MMS log " + e.getMessage());
					} finally {
						closeCursor(mms_cursor_addr);
					}
				}
			}
			
			closeCursor(sms_cursor);
			closeCursor(mms_cursor);
		}

		@Override
		public boolean deliverSelfNotifications() {
		    return true;
		}
	}
	
	private void closeCursor(Cursor cursor) {
		try {
			if (cursor != null) {
				cursor.close();
			}
		} catch( Exception e ) {
			new ClassEvents(TAG, "ERROR", "Could not close cursor " + e.getMessage());
		}
	}

	public String md5(String s) {
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i=0; i<messageDigest.length; i++)
	            hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
			new ClassEvents(TAG, "ERROR", "No MD5");
	    }
	    return "";
	}
}
